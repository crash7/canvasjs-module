# CanvasJS: Beautiful HTML5 JavaScript Charts
http://canvasjs.com/

Version: 1.9.8 (.1)

[CanvasJS official website](http://canvasjs.com)

## Why Choose CanvasJS Charts?

> CanvasJS Charts have a simple API and can render across devices including iPhone, iPad, Android, Windows Phone, Desktops, etc. This allows you to create rich dashboards that work across devices without compromising on maintainability or functionality of your web application. Graphs include several good looking themes and are 10x faster than conventional Flash / SVG based Charting Libraries – resulting in lightweight, beautiful and responsive dashboards. Checkout an overview of our JavaScript Charts.

## Disclaimer

This repository just extends the original code by including new features and several fixes.

The original code can be found at [http://canvasjs.com](http://canvasjs.com). It was released as [CC](http://creativecommons.org/licenses/by-nc/3.0/deed.en_US) for personal use and it needs to be licensed under commercial use - see terms [here](http://canvasjs.com/license-canvasjs/).
